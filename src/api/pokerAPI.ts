import { get, request } from "./index";

export interface Poker {
  title: string;
  description: string;
  url: string;
  images: any[];
  level: number;
}

export interface PokerProps extends Poker {
  _id: string;
}

export const getImage = (imageName: string) => get(`/getImage/${imageName}`);

export const createNewPoker = (poker: Poker) =>
  request("/createNewPoker", poker);

export const updatePoker = (poker: Partial<PokerProps>) =>
  request("/updatePoker", poker);

export const deletePoker = (id: string) => request("/deletePoker", id);
// return post("/deletePoker", id);

export const getPokers = () => get("/getPokers");
