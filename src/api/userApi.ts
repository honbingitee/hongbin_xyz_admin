import { request } from ".";
import { RequestBack } from "./types";

interface IQueryUser {
  sort?: { _id: -1 | 1 };
  limit?: number;
  skip?: number;
}

interface IUser {
  _id: string;
  username: string;
  role: number;
}

export interface BackUser {
  count: number;
  users: IUser[];
}

export const queryUser = (params: IQueryUser): RequestBack<BackUser> =>
  request("/queryUser", params);

interface IUpdateUserRole {
  _id: string;
  role: number;
}

export const updateUserRole = (
  params: IUpdateUserRole
): RequestBack<{ code: number; message: string }> =>
  request("/updateUserRole", params);
