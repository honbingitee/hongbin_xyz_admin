import { query, request } from ".";

export interface Sender {
  _id: string;
  username: string;
  role: number;
}

export interface Source {
  _id: string;
  name: string;
  description: string;
  owner: string;
  file: string;
  sender: [Sender];
}

interface IQuerySource {
  owner?: string;
}

export const querySource = (params: IQuerySource) =>
  query("/querySource", { params });

interface ICreateSource {
  name: string;
  description: string;
  file: File;
}

export const createSource = (params: ICreateSource) =>
  request("/createSource", params, { timeout: 1000 * 60 });

interface IDeleteSource {
  _id: string;
}

export const deleteSource = (params: IDeleteSource) =>
  request("/deleteSource", params);
interface IUpdateSource {
  name: string;
  description: string;
  file: File | null;
}

export const updateSource = (params: IUpdateSource) =>
  request("/updateSource", params);
