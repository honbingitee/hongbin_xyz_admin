import {
  NotificationError,
  NotificationSuccess,
} from "./../components/common/Notification";
import { getLocalJWT } from "./authApi";
import axios, { AxiosRequestConfig } from "axios";
import { baseURL } from "../constant/url";

axios.defaults.baseURL = baseURL;
axios.defaults.timeout = 5000;

axios.interceptors.request.use((config: AxiosRequestConfig) => {
  config.headers.Authorization = getLocalJWT();
  return config;
});

axios.interceptors.response.use(res => {
  const { data } = res;
  if (data.notification && data.message) {
    NotificationSuccess({ message: data.message });
  }
  return res;
});

export const { get, post } = axios;

export const query: (
  url: string,
  config?: AxiosRequestConfig | undefined
) => Promise<any> = async (url, config) =>
  await get(url, config)
    .then(res => res)
    // .then(res => res.data)
    .catch(err => {
      console.error("err", err.response);
      const message = err.response?.data?.message;
      if (message) NotificationError({ message });
      return { err: err.response };
    });

export const request: (
  url: string,
  data?: any,
  config?: AxiosRequestConfig | undefined
) => Promise<any> = (...props) => {
  return new Promise(resolve => {
    post(...props)
      .then(res => resolve(res))
      .catch(err => {
        // reject(err); // 配合await 使用catch时会遇到debug 😡
        console.error("request" + props[0] + " err", err, err.response);
        const message = err.response?.data?.message;
        if (message) NotificationError({ message });
        resolve({ err: err.response });
      });
  });
};
