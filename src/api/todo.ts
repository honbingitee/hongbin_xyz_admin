import { query, request } from ".";
import { RequestBack } from "./types";

export interface Todo {
  _id: string;
  name: string;
  result?: string;
}

export const queryTodoList = (): RequestBack<{
  code: number;
  message: string;
  todoList: Todo[];
}> => query("getTodoList");

export const addTodo = (params: {
  name: string;
}): RequestBack<{ _id: string; message: string }> =>
  request("/addTodo", params);

export const deleteTodo = (params: { id: string }) =>
  request("/deleteTodo", params);

export const updateTodo = (params: { _id: string; result: string }) =>
  request("/updateTodo", params);
