import { query } from ".";

export interface ITag {
  _id: string;
  name: string;
  count: number;
}

export const getAllBlogTags = async (): Promise<{ data: ITag[] }> =>
  await query("/getAllBlogTags");
