import { query } from ".";

export interface IDashboardDate {
  _id: string;
  newBlogs: number;
  newBrowse: number;
  newComment: number;
  totalBlogs: number;
  totalBrowse: number;
  totalComment: number;
}

export const getBlogCounts = () => query("/getBlogCounts");
export const getBlogTagsCounts = () => query("/getBlogTagsCounts");
export const getPokerCounts = () => query("/getPokerCounts");
export const getAllBrowseCounts = () => query("/getAllBrowseCounts");
export const getAllCommentCounts = () => query("/getAllCommentCounts");
export const getAllThumbsUpCounts = () => query("/getAllThumbsUpCounts");
export const getAllAccountCounts = () => query("/getAllAccountCounts");
export const getDashboardData = (
  day: number
): Promise<{ data: { data?: [IDashboardDate] } }> =>
  query("/getDashboardData", { params: { day } });
