import { request } from ".";
import { RequestBack } from "./types";
export interface IComment {
  _id: string;
  blogId: string; //属于哪个博客
  content: string; //评论内容 待实现markdown内容
  // createAt:string; 创建时间从_id前6位*1000取得
  senderId: string; //发送者的id
  thumbsUp: number; //点赞数 默认0
  thumbsDown: number; //点踩数 默认0
  reply?: string; //回复谁的评论:评论的id
  sender: [{ _id: string; username: string; role: number }];
  at?: string;
  violation: boolean;
}

interface IQuery {
  level?: number;
  limit?: number;
  skip?: number;
  sort?: { _id: -1 | 1 };
}

export const queryComment = (
  params: IQuery
): RequestBack<{ list: IComment[]; count: number }> =>
  request("/queryComment", params);

export interface IDeleteComment {
  _id: string;
  level: number;
}

export const deleteComment = (
  params: IDeleteComment
): RequestBack<{ message: string; code: number }> =>
  request("/deleteComment", params);

export interface IViolationComment extends IDeleteComment {
  violation: boolean;
}

export const violationComment = (
  params: IViolationComment
): RequestBack<{ message: string; code: number }> =>
  request("/violationComment", params);
