import { HashRouter, Redirect, Route, Switch } from "react-router-dom";
import Login from "./pages/login";
import Admin from "./pages/admin";
import NotFound from "./pages/notfonund";
import MD from "./pages/md";
import AuthContext from "./api/AuthContext";
import ProtectedRoute from "./components/common/ProtectedRoute";
import { GlobalStyle } from "./components/BUI";

function App() {
  console.log(process.env);
  return (
    <AuthContext>
      <GlobalStyle />
      <HashRouter>
        <Switch>
          <Redirect path='/' exact to='/admin' />
          <Route path='/login' component={Login} />
          <Route path='/md' component={MD} />
          <ProtectedRoute path='/admin' component={Admin} />
          <Route component={NotFound} />
        </Switch>
      </HashRouter>
    </AuthContext>
  );
}

export default App;
