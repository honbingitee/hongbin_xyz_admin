import PokerIcon from "../components/BUI/Icon/PokerIcon";
import { BlogManageIcon } from "../components/icon/blog";
import { TagIcon } from "../components/icon/tagManage";
import { AccountIcon } from "../components/icon/account";
import { DashboardIcon, TodoListSvg } from "../components/icon";
import { CommentOutlined, UserOutlined } from "@ant-design/icons";
import { SourceIcon } from "../components/icon/slidIcon";

export interface SideRoutesProps {
  key: string;
  icon?: JSX.Element;
  path?: string;
  title: string;
  children?: SideRoutesProps[];
}

export const AccountManages: SideRoutesProps[] = [
  {
    key: "updateUsername",
    path: "/admin/accountManage/updateUsername",
    title: "更新用户名",
  },
  {
    key: "updatePassword",
    path: "/admin/accountManage/updatePassword",
    title: "更新密码",
  },
  {
    key: "bindEMail",
    path: "/admin/accountManage/bindEMail",
    title: "绑定邮箱",
  },
  {
    key: "logoutAccount",
    path: "/admin/accountManage/logoutAccount",
    title: "注销账号",
  },
];

export const sideRoutes: SideRoutesProps[] = [
  {
    key: "dashboard",
    icon: <DashboardIcon />,
    path: "/admin/dashboard",
    title: "仪表盘",
  },
  {
    key: "pokerManage",
    icon: <PokerIcon />,
    path: "/admin/pokerManage",
    title: "扑克管理",
  },
  {
    key: "blogManage",
    icon: <BlogManageIcon />,
    path: "/admin/blogManage",
    title: "博客管理",
  },
  {
    key: "tagManage",
    icon: <TagIcon />,
    path: "/admin/tagManage",
    title: "标签管理",
  },
  {
    key: "commentManage",
    icon: <CommentOutlined />,
    path: "/admin/commentManage",
    title: "评论管理",
  },
  {
    key: "userManage",
    icon: <UserOutlined />,
    path: "/admin/userManage",
    title: "用户管理",
  },
  {
    key: "todoManage",
    icon: <TodoListSvg />,
    path: "/admin/todoManage",
    title: "代办事项",
  },
  {
    key: "sourceManage",
    icon: <SourceIcon />,
    path: "/admin/sourceManage",
    title: "资源管理",
  },
  {
    key: "accountManage",
    icon: <AccountIcon />,
    title: "账号管理",
    children: AccountManages,
  },
];
