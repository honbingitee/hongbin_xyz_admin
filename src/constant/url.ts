export const baseURL = process.env.REACT_APP_URL;
export const getImagePath = baseURL + "/getImage/";
export const downloadSourcePath = baseURL + "/downloadSource/";
