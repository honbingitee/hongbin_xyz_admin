/*
 * @Author: hongbin
 * @Date: 2021-09-25 17:27:25
 * @LastEditTime: 2021-09-25 17:36:00
 * @Description: RAS 加密
 */

import { JSEncrypt } from "jsencrypt";
import RASPublicKey from "./RASPublicKey";

const encrypt = new JSEncrypt();
encrypt.setPublicKey(RASPublicKey);

const RASEncrypt = (str: string) => encrypt.encrypt(str) as string;

export default RASEncrypt;
