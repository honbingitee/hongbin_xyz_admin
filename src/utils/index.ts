import { IDashboardDate } from "../api/dashboardAPI";
/**
 * @description: 传入 mongodb 自动生成的 _id 转化成Date对象
 * @param {string} _id
 * @return {Date} [Object Date]
 */
export function mongoIdToDate(_id: string): Date {
  return new Date(parseInt(`${_id.substr(0, 8)}`, 16) * 1000);
}

export interface LineChartData {
  name: string;
  count: number;
  date: string;
}

/**
 * 把后端返回的数据整合成antd charts需要的格式 返回 新增数据和昨日总数
 * @param   {array}         data IDashboardDate[]
 * @return  {array}              LineChartData[]
 */
export const formatDashboardData = (
  data: IDashboardDate[]
): [LineChartData[], any] => {
  const payload: LineChartData[] = [];
  console.log(data);
  for (const { _id, newBlogs, newBrowse, newComment } of data) {
    const date = mongoIdToDate(_id).toLocaleString();
    payload.push(
      { name: "新增博客", count: newBlogs, date },
      { name: "新增浏览量", count: newBrowse, date },
      { name: "新增评论", count: newComment, date }
    );
  }
  return [payload, data[data.length - 1]];
};

// export const formatDashboardData = (data: IDashboardDate[]): LineData[] => {
//   const browseData: LineData[] = [];
//   const commentData: LineData[] = [];
//   const blogData: LineData[] = [];
//   for (const { _id, newBlogs, newBrowse, newComment } of data) {
//     const date = mongoIdToDate(_id).toLocaleString();
//     blogData.push({ name: "新增博客", count: newBlogs, date });
//     browseData.push({ name: "新增浏览量", count: newBrowse, date });
//     commentData.push({ name: "新增评论", count: newComment, date });
//   }
//   return [...browseData, ...commentData, ...blogData];
// };
