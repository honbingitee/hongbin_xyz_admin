/*
 * @Author: your name
 * @Date: 2021-09-25 17:21:28
 * @LastEditTime: 2021-09-25 17:21:29
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /hongbin_xyz_admin/src/utils/RASPublicKey.ts
 */

const key = `-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDA9FBVPqESqji48ipLB8ybtGjm
kBe4GnYgYS4WKGAnZcKD2H3keF/8eLHjqcD9VBreyFHq1XaTFK/NSkcPobURyiZf
veplJ1cICnXbm+QdTlDPey3VlRjlrCeTuLKsFqbCMzcVAdcReDil/NujQmsDu+yv
Rghq/AhufUIxSGjSSwIDAQAB
-----END PUBLIC KEY-----`;

export default key;
