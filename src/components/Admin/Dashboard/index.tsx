import { FC, ReactElement, useEffect, useState, useCallback } from "react";
import { useHistory } from "react-router-dom";
import styled from "styled-components";
import * as DashboardAPI from "../../../api/dashboardAPI";
import useMount from "../../../hooks/useMount";
import { FlexDiv } from "../../BUI";
import { ActiveLateY, TextEllipsis } from "../../BUI/styled";
import { getDashboardData } from "../../../api/dashboardAPI";
import { formatDashboardData } from "../../../utils";
import DataLineCharts from "./DataLineCharts";
import TodayData from "./TodayData";

interface IProps {}

const Dashboard: FC<IProps> = (): ReactElement => {
  const { push } = useHistory();
  const [blogCounts, setBlogCounts] = useState(0);
  const [blogTagsCounts, setBlogTagsCounts] = useState(0);
  const [accountCounts, setAccountCounts] = useState(0);
  const [browseCounts, setBrowseCounts] = useState(0);
  const [commentCounts, setCommentCounts] = useState(0);
  const [thumbsUpCounts, setThumbsUpCounts] = useState(0);
  const [pokerCounts, setPokerCounts] = useState(0);
  const [dashboardData, setDashboardData] = useState<any>([]);
  const [todayData, setTodayData] = useState<any>([]);
  const [todayTotalData, setTodayTotalData] = useState<any>(undefined);

  const getDate = () => {
    getDashboardData(7).then(({ data }) => {
      console.log("getDashboardData:", data);
      if (data && Array.isArray(data.data) && data.data.length) {
        const [dashboard, yesterday] = formatDashboardData(data.data);
        setDashboardData(dashboard);
        setTodayTotalData(yesterday);
      }
    });
  };

  const Data = [
    { title: "总浏览量", counts: browseCounts },
    { title: "博客数", counts: blogCounts, to: "/admin/blogManage" },
    { title: "评论数", counts: commentCounts, to: "/admin/commentManage" },
    { title: "点赞数", counts: thumbsUpCounts },
    { title: "标签", counts: blogTagsCounts, to: "/admin/tagManage" },
    { title: "扑克", counts: pokerCounts, to: "/admin/pokerManage" },
    { title: "用户", counts: accountCounts, to: "/admin/userManage" },
  ];

  //TODO 应合并为一个请求处理
  const initData = useCallback(() => {
    DashboardAPI.getBlogCounts().then(res => {
      res.data && setBlogCounts(res.data);
    });
    DashboardAPI.getBlogTagsCounts().then(res => {
      res.data && setBlogTagsCounts(res.data);
    });
    DashboardAPI.getAllAccountCounts().then(res => {
      res.data && setAccountCounts(res.data);
    });
    DashboardAPI.getAllBrowseCounts().then(res => {
      res.data && setBrowseCounts(res.data);
    });
    DashboardAPI.getAllCommentCounts().then(res => {
      res.data && setCommentCounts(res.data);
    });
    DashboardAPI.getAllThumbsUpCounts().then(res => {
      res.data && setThumbsUpCounts(res.data);
    });
    DashboardAPI.getPokerCounts().then(res => {
      res.data && setPokerCounts(res.data);
    });
  }, []);

  useEffect(() => {
    if (todayTotalData && commentCounts && browseCounts && blogCounts) {
      const { totalBlogs, totalBrowse, totalComment } = todayTotalData;
      setTodayData([
        { type: "今日评论", value: commentCounts - totalComment },
        { type: "今日浏览", value: browseCounts - totalBrowse },
        { type: "今日博客", value: blogCounts - totalBlogs },
      ]);
    }
  }, [todayTotalData, commentCounts, browseCounts, blogCounts]);

  useMount(() => {
    initData();
    getDate();
  });

  return (
    <Container>
      <FlexDiv justify='space-evenly'>
        {Data.map(item => (
          <Card onClick={() => item.to && push(item.to)} key={item.title}>
            <strong>{item.counts}</strong>
            <strong>{item.title}</strong>
          </Card>
        ))}
      </FlexDiv>
      <FlexDiv>
        <DataLineCharts dashboardData={dashboardData} />
        <TodayData todayData={todayData} />
      </FlexDiv>
    </Container>
  );
};

export default Dashboard;

const Card = styled.div`
  background: linear-gradient(60deg, #00c3ff, #cc00ff);
  /* width: 100px;
  height: 90px; */
  flex: 1;
  max-width: 200px;
  border-radius: 60px 12px 20px 12px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  color: #fff;
  margin: 20px;
  cursor: pointer;
  user-select: none;
  transition: all 0.3s linear;
  box-shadow: 0 0 20px 0 #bbb;
  padding: 5px;
  font-size: 20px;
  white-space: nowrap;

  &:hover {
    transform: scale(1.1) translateY(-5px);
    box-shadow: 10px 10px 20px 1px #00000066;
  }

  ${ActiveLateY};
  & > strong:first-child {
    font-size: 30px;
    text-align: center;
    width: 100%;
    ${TextEllipsis};
  }
`;

const Container = styled.div`
  background: #fff;
  width: 100%;
  height: 100%;
  padding: 10px;

  & > div {
    height: 50%;
  }
  & > div:last-child {
    & > div:first-child {
      height: 100% !important;
      width: 70% !important;
    }
    & > div:last-child {
      height: 100% !important;
      width: 30% !important;
    }
  }
`;
