import { FC, ReactElement, memo } from "react";
// import { Line } from "@ant-design/charts";
import Line from "@ant-design/charts/es/plots/line";

interface IProps {
  dashboardData: any;
}

const DataLineCharts: FC<IProps> = ({ dashboardData }): ReactElement => {
  var config: any = {
    data: dashboardData,
    xField: "date",
    yField: "count",
    seriesField: "name",
    legend: { position: "bottom" },
    smooth: false,
    animation: {
      appear: {
        animation: "path-in",
        duration: 5000,
      },
    },
  };

  return <Line {...config} />;
};

export default memo(DataLineCharts);
