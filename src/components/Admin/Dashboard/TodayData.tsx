import { FC, ReactElement, memo } from "react";
import Column from "@ant-design/charts/es/plots/column";

interface IProps {
  todayData: any;
}

const TodayData: FC<IProps> = ({ todayData }): ReactElement => {
  var config = {
    data: todayData,
    xField: "type",
    yField: "value",
    seriesField: "",
    color: function color(_ref: any) {
      var type = _ref.type;
      if (type === "今日浏览") {
        return "#51f";
      } else if (type === "今日新增博客") {
        return "#000";
      }
      return "#faa";
    },
    legend: false,
    xAxis: {
      label: {
        autoHide: true,
        autoRotate: false,
      },
    },
  };

  return <Column {...config} />;
};

export default memo(TodayData);
