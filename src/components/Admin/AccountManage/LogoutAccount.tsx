import { FC, ReactElement } from "react";
import { Wrap } from "./UpdateUsername";

interface IProps {}

const LogoutAccount: FC<IProps> = (): ReactElement => {
  return (
    <Wrap>
      <h3>不可能, 绝对不可能</h3>
    </Wrap>
  );
};

export default LogoutAccount;
