import { Route, Switch } from "react-router-dom";
import { Container } from "../TagManage";
import BindEMail from "./BindEMail";
import LogoutAccount from "./LogoutAccount";
import UpdatePassword from "./UpdatePassword";
import UpdateUsername from "./UpdateUsername";

const AccountManage = () => {
  return (
    <Container
      style={{
        display: "flex",
        justifyContent: "center",
        position: "relative",
      }}
    >
      <Switch>
        <Route
          path='/admin/accountManage/updateUsername'
          component={UpdateUsername}
        />
        <Route
          path='/admin/accountManage/updatePassword'
          component={UpdatePassword}
        />
        <Route path='/admin/accountManage/bindEMail' component={BindEMail} />
        <Route
          path='/admin/accountManage/logoutAccount'
          component={LogoutAccount}
        />
      </Switch>
    </Container>
  );
};

export default AccountManage;
