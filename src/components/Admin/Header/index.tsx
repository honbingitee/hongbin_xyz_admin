import { FC, ReactElement } from "react";
import { Layout, Menu } from "antd";
import { Row, Col } from "antd";
import { EditOutlined } from "@ant-design/icons";
import { useAuth } from "../../../api/AuthContext";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { AccountManages } from "../../../constant/routers";

interface IProps {}

const Header: FC<IProps> = (): ReactElement => {
  const { auth, signOut } = useAuth();
  return (
    <LayoutHeader>
      <Row justify='space-between'>
        <Col>
          <Menu theme='dark' mode='horizontal'>
            <Menu.Item icon={<EditOutlined />} key='newBlog'>
              <Link to='/md'>写博客</Link>
            </Menu.Item>
            <Menu.Item key='aboutPageManage'>
              <Link to='/md?blog=613c825ec3518d54ad76bf99'>关于页管理</Link>
            </Menu.Item>
          </Menu>
        </Col>
        <Col style={{ backgroundColor: "#fff" }}>
          <Menu theme='dark' mode='horizontal'>
            <Menu.SubMenu key='AuthName' title={auth.username}>
              <Menu.ItemGroup title='账号管理'>
                {AccountManages.map(item => (
                  <Menu.Item key={item.key}>
                    <Link to={item.path as string}>{item.title}</Link>
                  </Menu.Item>
                ))}
              </Menu.ItemGroup>
              <Menu.Item key='sign_out' onClick={signOut}>
                退出登陆
              </Menu.Item>
            </Menu.SubMenu>
          </Menu>
        </Col>
      </Row>
    </LayoutHeader>
  );
};

export default Header;

const LayoutHeader = styled(Layout.Header)`
  /* background: var(--primary-color); */
  & div,
  li {
    /* background: var(--primary-color) !important; */
  }
`;
