import { FC, ReactElement } from "react";
import { FlexDiv } from "../../BUI";
import { getImagePath } from "../../../constant/url";
import styled from "styled-components";
import { flexCenter } from "../../BUI/styled";
import { RedoOutlined } from "@ant-design/icons";

interface IProps {
  fileList: Array<string>;
  update?: boolean;
  updateImageCropper: (index: number) => () => void;
  onDeleteFileItem: (index: number) => () => void;
  appendChildren: JSX.Element;
}

const ImageList: FC<IProps> = ({
  fileList,
  update,
  updateImageCropper,
  onDeleteFileItem,
  appendChildren,
}): ReactElement => {
  //需要应对值为后端返回的文件名和base64两种情况
  return (
    <FlexDiv style={{ marginBottom: 5 }}>
      {fileList.map((file: string, index: any) => (
        <ImageContainer key={index}>
          <img
            src={
              file.substring(0, 50).indexOf(".png") === -1
                ? file
                : `${getImagePath}${file}`
            }
            alt=''
          />
          <span onClick={onDeleteFileItem(index)}>删除</span>
          <UpdateFileMask onClick={updateImageCropper(index)}>
            <RedoOutlined />
          </UpdateFileMask>
        </ImageContainer>
      ))}
      {appendChildren}
    </FlexDiv>
  );
};

export default ImageList;

const UpdateFileMask = styled.div`
  position: absolute;
  left: 0;
  height: 50%;
  background-color: #000000aa;
  width: 100%;
  ${flexCenter};
  color: #fff;
  font-size: 20px;
  transition-property: opacity;
  transition-duration: 0.3s;
  transition-timing-function: ease-in;
  opacity: 0;
  bottom: 0;
  cursor: pointer;
`;

const ImageContainer = styled.div`
  width: 100px;
  height: 100px;
  position: relative;
  margin-right: 10px;
  margin-bottom: 10px;

  & > img {
    width: inherit;
    height: inherit;
  }

  &:hover span {
    opacity: 1;
  }

  &:hover ${UpdateFileMask} {
    opacity: 1;
  }

  & > span {
    font-size: 8px;
    position: absolute;
    width: 40px;
    height: 20px;
    border-radius: 10px;
    background-color: #fff;
    box-shadow: 0 0 10px 1px #ccc;
    top: 0;
    right: 0;
    transform: translate(8px, -8px);
    text-align: center;
    line-height: 20px;
    transition: opacity 0.2s linear;
    opacity: 0;
    cursor: pointer;
  }
`;
