import { memo } from "react";
import { FC, ReactElement, useMemo } from "react";
import styled, { css } from "styled-components";
import { useAuth } from "../../../api/AuthContext";
import { BlogStatus, BlogType, Sort } from "../../../api/blogApi";
import { FlexDiv } from "../../BUI";
import { ActiveLateY, flexCenter } from "../../BUI/styled";
import {
  SortAscendingOutlined,
  SortDescendingOutlined,
} from "@ant-design/icons";
interface IProps {
  counts: number;
  blogStatus: BlogStatus;
  toggleBlogStatus: (status: BlogStatus) => () => void;
  self: boolean;
  toggleSelf: () => void;
  blogType: BlogType | "";
  toggleBlogType: (type: BlogType) => () => void;
  sort: Sort;
  toggleSortField: (field: keyof Sort) => () => void;
  toggleSortValue: (value: Sort[string]) => () => void;
}

const Head: FC<IProps> = ({
  counts,
  blogStatus,
  toggleBlogStatus,
  self,
  toggleSelf,
  blogType,
  toggleBlogType,
  sort,
  toggleSortField,
  toggleSortValue,
}): ReactElement => {
  const { auth } = useAuth();
  const blogStatusList = useMemo(
    () => [
      BlogStatus.ALL,
      BlogStatus.PUBLIC,
      BlogStatus.PRIVATE,
      BlogStatus.DRAFT,
    ],
    []
  );
  const blogTypeList = useMemo(() => [BlogType.ORIGINAL, BlogType.REPRINT], []);

  const sortArr: [keyof Sort, Sort[string]] = useMemo(
    () => Object.entries(sort)[0],
    [sort]
  );

  return (
    <Header>
      {blogStatusList.map(status => (
        <span
          key={status}
          data-active={blogStatus === status}
          onClick={toggleBlogStatus(status)}
        >
          {status}
          {blogStatus === status ? ` (${counts})` : ""}
        </span>
      ))}
      <FlexDiv flex={1} justify='flex-end'>
        {blogTypeList.map(type => (
          <span
            key={type}
            data-active={blogType === type}
            onClick={toggleBlogType(type)}
          >
            {type}
          </span>
        ))}
        {auth.role === 2 ? (
          <span data-active={self} onClick={toggleSelf}>
            只看自己
          </span>
        ) : null}
      </FlexDiv>
      <SortBox>
        <div data-left-round />
        排序:
        <span
          onClick={toggleSortField("createAt")}
          data-active={sortArr[0] === "createAt"}
        >
          创建时间
        </span>
        <span
          onClick={toggleSortField("updateAt")}
          data-active={sortArr[0] === "updateAt"}
        >
          更新时间
        </span>
        <SortAscendingOutlined
          onClick={toggleSortValue(-1)}
          data-active={sortArr[1] === -1}
        />
        <SortDescendingOutlined
          onClick={toggleSortValue(1)}
          data-active={sortArr[1] === 1}
        />
      </SortBox>
    </Header>
  );
};

export default memo(Head);

const SortBox = styled(FlexDiv)`
  position: absolute;
  right: 0px;
  bottom: -25px;
  height: 26px;
  width: 410px;
  background: inherit;
  border-bottom-left-radius: 30px;
  padding: 0 0 2px 0;

  && {
    //提升优先级
    & > span {
      padding: 0 16px;
      ${flexCenter};
    }
  }

  & > div[data-left-round] {
    background: inherit;
    transform: translateX(-35px);
    width: 55px;
    height: 20px;
    position: relative;
    overflow: hidden;
    -webkit-clip-path: polygon(
      0 0,
      100% 0,
      100% 86%,
      77% 87%,
      59% 47%,
      47% 29%,
      27% 10%,
      0 0
    );
    clip-path: polygon(
      0 0,
      100% 0,
      100% 86%,
      77% 87%,
      59% 47%,
      47% 29%,
      27% 10%,
      0 0
    );

    /* &::before {
      content: "";
      position: absolute;
      top: 0;
      left: 0;
      width: inherit;
      height: 50px;
      border-top-right-radius: 100%;
      background: #fff;
    } */
  }
`;

const ItemHover = css`
  background: #777;
  box-shadow: 4px 5px 20px 0px #7266665c;
`;

const ItemActive = css`
  background: #000;
  box-shadow: 0 0 10px 0 #272727;
`;

const Header = styled.div`
  width: inherit;
  height: 50px;
  background: #51f;
  position: -webkit-sticky;
  position: sticky;
  top: 0;
  display: flex;
  z-index: 1;
  align-items: center;
  color: #fff;
  font-weight: bold;
  /* box-shadow: 0 1px 5px 0px #666; */
  margin-bottom: 20px;

  & span {
    padding: 10px 30px;
    margin: 0 5px;
    cursor: pointer;
    transition: all 0.3s linear;
    background: #ffffff2f;
    user-select: none;

    &:hover {
      ${ItemHover};
    }
    ${ActiveLateY};
    @media screen and (max-width: 1200px) {
      padding: 10px;
    }
  }
  & span[data-active="true"] {
    ${ItemActive};
  }
`;
