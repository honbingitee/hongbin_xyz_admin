import { useState } from "react";
import { FC, ReactElement, useEffect } from "react";
import styled from "styled-components";
import { getAllBlogTags, ITag } from "../../../api/tagApi";
import { Table } from "antd";

interface IProps {}
const columns = [
  {
    title: "名称",
    dataIndex: "name",
    key: "name",
  },
  {
    title: "数量",
    dataIndex: "count",
    key: "count",
    sorter: {
      compare: (a: ITag, b: ITag) => a.count - b.count,
      multiple: 2,
    },
  },
];
const TagManage: FC<IProps> = (): ReactElement => {
  const [tags, setTags] = useState<ITag[]>([]);

  useEffect(() => {
    getAllBlogTags().then(({ data }) => {
      console.log("tags:", data);
      data && setTags(data);
    });
  }, []);

  return (
    <Container>
      <TagList>
        <Table
          rowKey='_id'
          columns={columns}
          dataSource={tags}
          scroll={{ y: 500 }}
          pagination={{ pageSize: 20, position: ["bottomCenter"] }}
        />
      </TagList>
    </Container>
  );
};

export default TagManage;

const TagList = styled.div`
  position: relative;
`;

export const Container = styled.div`
  background: #fff;
  width: 100%;
  height: inherit;
  overflow: hidden;
`;
