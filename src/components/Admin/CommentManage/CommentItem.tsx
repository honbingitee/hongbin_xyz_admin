import { FC, ReactElement, useState, memo } from "react";
import styled from "styled-components";
import {
  IDeleteComment,
  IComment,
  violationComment,
} from "../../../api/commentAPI";
import { Button, Popover } from "antd";
import { mongoIdToDate } from "../../../utils";
import { NotificationSuccess } from "../../common/Notification";

interface IProps {
  comment: IComment;
  handleDeleteComment: (params: IDeleteComment) => Promise<boolean | void>;
}

const CommentItem: FC<IProps> = ({
  comment,
  handleDeleteComment,
}): ReactElement => {
  const [loading, setLoading] = useState("");
  const [violation, setViolation] = useState(comment.violation);
  const commentParams = { level: comment.at ? 2 : 1, _id: comment._id };

  const handleViolationComment = async (
    params: Parameters<typeof violationComment>[number]
  ) => {
    setLoading("violation");
    const { data } = await violationComment(params);
    if (data && data.code === 0) {
      NotificationSuccess({ message: data.message });
      setViolation(params.violation);
    }
    setLoading("");
  };

  const deleteContent =
    loading === "violation" ? (
      <span>等待其他请求...</span>
    ) : (
      <>
        <span>请谨慎处理 不可撤回</span>
        <br />
        <Button
          loading={loading === "del"}
          onClick={async () => {
            setLoading("del");
            const result = await handleDeleteComment(commentParams);
            if (!result) setLoading("");
          }}
        >
          确定删除
        </Button>
      </>
    );

  return (
    <Container>
      <span>{comment.content}</span>
      <div>
        <div>
          <span>{mongoIdToDate(comment._id).toLocaleString()}</span>
          <span>发送者:{comment.sender[0].username}</span>
          <span style={{ background: violation ? "#f00" : "" }}>
            {violation ? "不可见" : "可见"}
          </span>
        </div>
        <div>
          <Popover
            content={
              loading === "del" ? (
                <span>等待其他请求...</span>
              ) : (
                <Button
                  loading={loading === "violation"}
                  onClick={() => {
                    handleViolationComment({
                      ...commentParams,
                      violation: !violation,
                    });
                  }}
                >
                  {violation ? "解封违规评论" : "设为违规评论"}
                </Button>
              )
            }
            title={violation ? "解封评论" : "设为违规评论"}
          >
            <Button type='dashed' danger={violation}>
              {violation ? "解封评论" : "设为违规评论"}
            </Button>
          </Popover>
          <Popover content={deleteContent} title='删除评论'>
            <Button type='primary'>删除评论</Button>
          </Popover>
        </div>
      </div>
    </Container>
  );
};

export default memo(CommentItem);

const Container = styled.div`
  margin-top: 1rem;
  padding: 0.5rem;
  border-radius: 0.2rem;
  box-shadow: 5px 5px 10px #d6d6d6, -5px -5px 10px #ffffff;
  transition: transform 0.3s linear, box-shadow 0.3s linear;
  display: flex;
  flex-direction: column;

  & > span {
    background: #d6d6d6;
    flex: 1;
    padding: 1rem;
    border-radius: inherit;
  }

  & > div {
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-top: 5px;

    & > div > span {
      background-color: #ddd;
      border-radius: 1rem;
      padding: 0.2rem 0.5rem;
      font-size: 0.8rem;
    }

    & > div > * {
      margin-right: 5px;
    }
  }

  &:hover {
    transform: translateY(-2px);
    box-shadow: 5px 5px 10px #d6d6d6, -5px -5px 10px #eeeeee;
  }
`;
