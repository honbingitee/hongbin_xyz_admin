import { FC, ReactElement, useCallback, useEffect, useState } from "react";
import styled from "styled-components";
import { IComment, queryComment, deleteComment } from "../../../api/commentAPI";
import CommentItem from "./CommentItem";
import { NotificationSuccess } from "../../common/Notification";
import { Switch } from "antd";
import {
  SortAscendingOutlined,
  SortDescendingOutlined,
} from "@ant-design/icons";
import Pagination from "../../BUI/Pagination";

interface IProps {}

const CommentManage: FC<IProps> = (): ReactElement => {
  const [level, setLevel] = useState(1);
  const [loading, setLoading] = useState("");
  const [comments, setComments] = useState<{
    list: IComment[];
    count: number;
  }>({ list: [], count: 0 });
  const [sort, setSort] = useState("DESC");
  const [page, setPage] = useState(1);

  const query = useCallback(async () => {
    const { data } = await queryComment({
      level,
      sort: { _id: sort === "DESC" ? -1 : 1 },
      skip: (page - 1) * 20,
    });
    setLoading("");
    if (!data) return;
    setComments(data);
  }, [level, sort, page]);

  useEffect(() => {
    query();
  }, [query]);

  const handleDeleteComment = useCallback(
    async (params: Parameters<typeof deleteComment>[number]) => {
      const { data } = await deleteComment(params);
      if (data && data.code === 0) {
        NotificationSuccess({ message: data.message });
        setComments(prev => ({
          list: prev.list.filter(comment => comment._id !== params._id),
          count: prev.count - 1,
        }));
        return true;
      }
    },
    []
  );

  return (
    <Container>
      <OptionNav>
        <span>共有（{comments.count}）条记录</span>
        <Switch
          checkedChildren='一级评论'
          unCheckedChildren='二级评论'
          defaultChecked
          loading={loading === "switchLevel"}
          onChange={checked => {
            setLoading("switchLevel");
            setLevel(checked ? 1 : 2);
          }}
        />
        <Switch
          checkedChildren={
            <>
              <SortDescendingOutlined />
              降序
            </>
          }
          unCheckedChildren={
            <>
              <SortAscendingOutlined />
              生序
            </>
          }
          defaultChecked
          loading={loading === "switchSort"}
          onChange={checked => {
            setLoading("switchSort");
            setSort(checked ? "DESC" : "ASC");
          }}
        />
      </OptionNav>
      <div>
        {comments.list.map(comment => (
          <CommentItem
            key={comment._id}
            comment={comment}
            handleDeleteComment={handleDeleteComment}
          />
        ))}
        <Pagination
          current={page}
          pageCount={20}
          total={comments.count}
          onChange={setPage}
        />
      </div>
    </Container>
  );
};

export default CommentManage;

const OptionNav = styled.section`
  display: flex;
  align-items: center;
  height: 3rem;
  /* background: var(--primary-color); */
  background: linear-gradient(60deg, #00c3ff, #cc00ff);
  padding: 1rem;
  color: #fff;
  & > * {
    margin-right: 1rem;
  }
`;

const Container = styled.div`
  background: #fff;
  height: 100%;
  display: flex;
  flex-direction: column;

  & > div {
    flex: 1;
    overflow-y: scroll;
    padding: 1rem;
  }
`;
