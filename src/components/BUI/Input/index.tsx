// import { FC, ReactElement } from "react";
import styled from "styled-components";

// interface IProps {}

// const Input: FC<IProps> = (): ReactElement => {
//   return <div></div>;
// };

// export default Input;

const TextInput = styled.input.attrs(props => ({
  type: props.type || "text",
}))<{ width?: string }>`
  padding: 0.1rem 0.3rem;
  border: none;
  border-top-left-radius: 0.2rem;
  border-top-right-radius: 0.2rem;
  background-color: #b8b3b360;
  border-bottom: 0.1rem solid #51f;
  min-height: 2rem;
  max-height: 5rem;
  width: ${props => props.width || "10rem"};
  outline: none;
  font-size: 1rem;
`;

export default TextInput;
