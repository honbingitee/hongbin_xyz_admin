import styled, { keyframes } from "styled-components";

export const Container = styled.div`
  width: 12rem;
  position: relative;
  margin: 0.5rem;
  user-select: none;
`;

const animateText = `
  0%{
    transform: scale(0.9);
  }
  100%{
    transform: scale(1);
  }
`;

const showAnimate = keyframes`
  ${animateText}
`;

export const DayContainer = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
`;

export const Wrapper = styled.div<{
  width?: string;
  noShadow?: boolean;
  row?: boolean;
}>`
  overflow: hidden;
  width: ${({ width }) => width || "12rem"};
  height: 15rem;
  padding: 0.5rem;
  padding-bottom: 0;
  background-color: #fff;
  border-radius: 0.2rem;
  position: absolute;
  top: 0;
  left: 0;
  display: flex;
  flex-direction: ${({ row }) => (row ? "row" : "column")};
  animation: ${showAnimate} 0.1s ease-out;
  ${({ noShadow }) =>
    noShadow
      ? undefined
      : "box-shadow: 5px 5px 14px #e8e8e8, -5px -5px 14px #ffffff;"}
`;

export const DateTimeShow = styled.div`
  width: 12rem;
  /* height: 2rem; */
  font-weight: bold;
  overflow: hidden;
  letter-spacing: 1px;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;

  & > svg {
    transition-property: opacity, transform;
    transition-timing-function: linear;
    transition-duration: 0.4s;
    opacity: 0;
    margin-left: 2px;
    transform: translateX(0.5rem);
  }

  &:hover p {
    color: #51f;
    letter-spacing: 0;
  }

  &:hover {
    svg {
      opacity: 1;
      transform: translateX(0rem);
    }
  }
`;

export const DayItem = styled.div<{ pointer?: boolean }>`
  width: 14.28%;
  height: 1.5rem;
  font-size: 0.5rem;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: ${({ pointer }) => (pointer ? "pointer" : undefined)};

  &:hover {
    transition: all 0.2s linear;
    background-color: #eeeeee83;
  }
`;

export const DateGridContainer = styled.div`
  flex: 1;
  display: flex;
  flex-wrap: wrap;
  align-items: center;
`;

export const TimeIcon = () => (
  <svg
    viewBox='0 0 1024 1024'
    version='1.1'
    xmlns='http://www.w3.org/2000/svg'
    p-id='5943'
    width='1rem'
    height='1rem'
  >
    <path
      d='M510.138182 531.316364a23.272727 23.272727 0 0 1-18.850909-9.541819l-190.138182-259.956363a23.179636 23.179636 0 0 1 5.12-32.581818 23.179636 23.179636 0 0 1 32.581818 5.12l190.138182 259.956363a23.179636 23.179636 0 0 1-18.850909 37.003637z'
      fill='#5511ff'
      p-id='5944'
    ></path>
    <path
      d='M707.723636 531.316364h-197.818181c-12.8 0-23.272727-10.472727-23.272728-23.272728s10.472727-23.272727 23.272728-23.272727h197.818181c12.8 0 23.272727 10.472727 23.272728 23.272727s-10.472727 23.272727-23.272728 23.272728z'
      fill='#5511ff'
      p-id='5945'
    ></path>
    <path
      d='M869.934545 915.316364h-199.214545c-12.8 0-23.272727-10.472727-23.272727-23.272728s10.472727-23.272727 23.272727-23.272727h199.214545l0.698182-720.756364-720.523636-0.698181-0.698182 720.756363 360.727273 0.698182c12.8 0 23.272727 10.472727 23.272727 23.272727s-10.472727 23.272727-23.272727 23.272728H150.109091c-26.065455 0-47.243636-21.178182-47.243636-47.243637V148.014545c0-26.065455 21.178182-47.243636 47.243636-47.243636h720.058182c26.065455 0 47.243636 21.178182 47.243636 47.243636V868.072727c0 26.065455-21.410909 47.243636-47.476364 47.243637z'
      fill='#5511ff'
      p-id='5946'
    ></path>
  </svg>
);

export const CloseIcon = ({ onClick }: { onClick: () => void }) => (
  <svg
    style={{ width: "1.5rem", height: "1.5rem", cursor: "pointer" }}
    onClick={onClick}
    viewBox='0 0 1024 1024'
    version='1.1'
    xmlns='http://www.w3.org/2000/svg'
    p-id='2208'
  >
    <path
      d='M571.7 514.1l164.9-164.9c16.3-16.3 16.3-43.1 0-59.4-16.3-16.3-43.1-16.3-59.4 0L512.3 454.7 347.4 289.8c-16.3-16.3-43.1-16.3-59.4 0-16.3 16.3-16.3 43.1 0 59.4l164.9 164.9L288 679c-16.3 16.3-16.3 43.1 0 59.4 16.3 16.3 43.1 16.3 59.4 0l164.9-164.9 164.9 164.9c16.3 16.3 43.1 16.3 59.4 0 16.3-16.3 16.3-43.1 0-59.4L571.7 514.1z'
      p-id='2209'
      fill='#5511ff'
    ></path>
  </svg>
);

export const LeftIcon = () => (
  <svg
    viewBox='0 0 1024 1024'
    version='1.1'
    xmlns='http://www.w3.org/2000/svg'
    p-id='1191'
    width='16'
    height='16'
  >
    <path
      d='M363.840919 472.978737C336.938714 497.358861 337.301807 537.486138 364.730379 561.486138L673.951902 832.05497C682.818816 839.813519 696.296418 838.915012 704.05497 830.048098 711.813519 821.181184 710.915012 807.703582 702.048098 799.94503L392.826577 529.376198C384.59578 522.174253 384.502227 511.835287 392.492414 504.59418L702.325747 223.807723C711.056111 215.895829 711.719614 202.404616 703.807723 193.674252 695.895829 184.943889 682.404617 184.280386 673.674253 192.192278L363.840919 472.978737Z'
      p-id='1192'
      fill='#5511ff'
    ></path>
  </svg>
);

export const RightIcon = () => (
  <svg
    viewBox='0 0 1024 1024'
    version='1.1'
    xmlns='http://www.w3.org/2000/svg'
    p-id='1359'
    width='16'
    height='16'
  >
    <path
      d='M642.174253 504.59418C650.164439 511.835287 650.070886 522.174253 641.84009 529.376198L332.618569 799.94503C323.751654 807.703582 322.853148 821.181184 330.611697 830.048098 338.370249 838.915012 351.847851 839.813519 360.714765 832.05497L669.936288 561.486138C697.36486 537.486138 697.727953 497.358861 670.825747 472.978737L360.992414 192.192278C352.26205 184.280386 338.770837 184.943889 330.858944 193.674252 322.947053 202.404616 323.610556 215.895829 332.340919 223.807723L642.174253 504.59418Z'
      p-id='1360'
      fill='#5511ff'
    ></path>
  </svg>
);

export const MikeTrueIcon = () => (
  <svg
    viewBox='0 0 1024 1024'
    version='1.1'
    xmlns='http://www.w3.org/2000/svg'
    p-id='4538'
    width='1rem'
    height='1rem'
  >
    <path
      d='M1014.875 192.6L331.775 875.7c-12.3 12.3-32.5 12.3-44.8 0-12.3-12.3-12.3-32.5 0-44.8L970.075 147.8c12.3-12.3 32.5-12.3 44.8 0 12.3 12.3 12.3 32.5 0 44.8z'
      fill='#5511ff'
      p-id='4539'
    ></path>
    <path
      d='M331.375 876.1c-12.5 12.5-32.8 12.5-45.3 0L9.375 599.4c-12.5-12.5-12.5-32.8 0-45.3s32.8-12.5 45.3 0l276.7 276.7c12.4 12.5 12.4 32.8 0 45.3z'
      fill='#5511ff'
      p-id='4540'
    ></path>
  </svg>
);

export const WeekContainer = styled.div`
  display: flex;
  justify-content: space-around;
`;

export const Operation = styled.div`
  position: relative;
  height: 1.5rem;
  margin-bottom: 0.1rem;
  color: #51f;
`;

export const ButtonGroup = styled.div`
  display: flex;
  flex: 1;
  justify-content: flex-end;

  svg {
    width: 1rem;
    height: 1rem;
  }
`;

export const Head = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 1.5rem;
  margin-bottom: 0.5rem;

  & > svg {
    transition: transform 0.1s linear;
    &:hover {
      transform: rotate(90deg);
    }
  }
`;

export const Content = styled.div`
  flex: 1;
  position: relative;
  display: flex;
  overflow: hidden;
`;

export const CloseBtn = ({
  onClose,
  style,
}: {
  onClose?: () => void;
  style?: any;
}) => (
  <svg
    onClick={onClose}
    viewBox='0 0 1024 1024'
    version='1.1'
    xmlns='http://www.w3.org/2000/svg'
    p-id='3051'
    width='1rem'
    height='1rem'
    style={style}
  >
    <path
      d='M948.534738 399.176444L574.704888 26.011773a88.690356 88.690356 0 0 0-125.496853 0L75.378186 399.176444A88.690356 88.690356 0 0 0 137.904887 551.280404h211.96995l44.345178 369.395332a117.736447 117.736447 0 0 0 233.699087 0l44.345178-369.395332h212.191676A88.690356 88.690356 0 0 0 948.534738 399.176444z'
      fill='#5511ff'
      p-id='3052'
    ></path>
  </svg>
);

export const AbsolutePopup = styled.div<{ bottom: string | number }>`
  position: absolute;
  background-color: #fff;
  width: 100%;
  height: 100%;
  transition: bottom 0.3s ease-in;
  bottom: ${({ bottom }) => bottom || 0};
  display: flex;
  flex-wrap: wrap;
`;

export const OperationBar = styled.div<{ status: boolean }>`
  position: absolute;
  height: 100%;
  background-color: #fff;
  width: 100%;
  top: ${({ status }) => (status ? 0 : "100%")};
  opacity: ${({ status }) => (status ? 1 : 0)};
  transition: all 0.2s 0s ease-out;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const ShrinkIcon = () => (
  <svg
    viewBox='0 0 1024 1024'
    version='1.1'
    xmlns='http://www.w3.org/2000/svg'
    p-id='2730'
    width='1rem'
    height='1rem'
  >
    <path
      d='M604.16 235.52a30.72 30.72 0 0 1 30.69952 29.568L634.88 266.24v122.88h122.88a30.72 30.72 0 0 1 30.69952 29.568L788.48 419.84a30.72 30.72 0 0 1-29.568 30.69952L757.76 450.56h-153.6a30.72 30.72 0 0 1-30.69952-29.568L573.44 419.84V266.24a30.72 30.72 0 0 1 30.72-30.72z'
      fill='#5511ff'
      p-id='2731'
    ></path>
    <path
      d='M797.47584 183.07584a30.72 30.72 0 0 1 44.34432 42.51136l-0.896 0.93696-215.04 215.04a30.72 30.72 0 0 1-44.34432-42.51136l0.896-0.93696 215.04-215.04zM419.84 573.44a30.72 30.72 0 0 1 30.69952 29.568L450.56 604.16v153.6a30.72 30.72 0 0 1-61.41952 1.152L389.12 757.76v-122.88H266.24a30.72 30.72 0 0 1-30.69952-29.568L235.52 604.16a30.72 30.72 0 0 1 29.568-30.69952L266.24 573.44h153.6z'
      fill='#5511ff'
      p-id='2732'
    ></path>
    <path
      d='M398.11584 582.43584a30.72 30.72 0 0 1 44.34432 42.51136l-0.896 0.93696-215.04 215.04a30.72 30.72 0 0 1-44.34432-42.51136l0.896-0.93696 215.04-215.04zM757.76 573.44a30.72 30.72 0 0 1 1.152 61.41952L757.76 634.88h-122.88v122.88a30.72 30.72 0 0 1-29.568 30.69952L604.16 788.48a30.72 30.72 0 0 1-30.69952-29.568L573.44 757.76v-153.6a30.72 30.72 0 0 1 29.568-30.69952L604.16 573.44h153.6z'
      fill='#5511ff'
      p-id='2733'
    ></path>
    <path
      d='M582.43584 582.43584a30.72 30.72 0 0 1 42.51136-0.896l0.93696 0.896 215.04 215.04a30.72 30.72 0 0 1-42.51136 44.34432l-0.93696-0.896-215.04-215.04a30.72 30.72 0 0 1 0-43.44832zM419.84 235.52a30.72 30.72 0 0 1 30.69952 29.568L450.56 266.24v153.6a30.72 30.72 0 0 1-29.568 30.69952L419.84 450.56H266.24a30.72 30.72 0 0 1-1.152-61.41952L266.24 389.12h122.88V266.24a30.72 30.72 0 0 1 29.568-30.69952L419.84 235.52z'
      fill='#5511ff'
      p-id='2734'
    ></path>
    <path
      d='M183.07584 183.07584a30.72 30.72 0 0 1 42.51136-0.896l0.93696 0.896 215.04 215.04a30.72 30.72 0 0 1-42.51136 44.34432l-0.93696-0.896-215.04-215.04a30.72 30.72 0 0 1 0-43.44832z'
      fill='#5511ff'
      p-id='2735'
    ></path>
  </svg>
);
