import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
  html,body,#root {
    height: 100%;
    margin:0;
    padding:0;
    font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
  }
  div,nav,input{
    box-sizing: border-box;
    padding: 0;
    margin: 0;
  }
  @media (max-width: 750px){
    #root::after{
      content: '请从PC端访问😁';
      position: absolute;
      width: 100vw;
      height: 100vh;
      background:linear-gradient(60deg, #faa,#00f);
      color:#fff;
      top:0;
      left:0;
      z-index:1;
      line-height: 100vh;
      text-align: center;
      font-weight: bold;
      font-size: 3rem;
    }
  }
`;

export default GlobalStyle;
