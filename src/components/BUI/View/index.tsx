import styled from "styled-components";
import { AlignItems, TPosition, JustifyContentProps } from "../types";

export const Div = styled.div`
  background-color: ${props => props.theme?.bg};
`;

export const FlexDiv = styled(Div)<{
  justify?: JustifyContentProps;
  items?: AlignItems;
  width?: string;
  height?: string;
  space?: string;
  nowrap?: boolean;
  flex?: number;
}>`
  display: flex;
  flex: ${props => props.flex};
  justify-content: ${props => props.justify};
  align-items: ${props => props.items};
  flex-wrap: ${props => (props.nowrap ? "nowrap" : "wrap")};
  width: ${props => props.width};
  height: ${props => props.height};
  & > * {
    margin-left: ${props => props.space};
    margin-right: ${props => props.space};
  }
`;

const CenterBox = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
`;

export default CenterBox;

export const ItemCenterBox = styled.div`
  display: flex;
  align-items: center;
`;

export const BetweenBox = styled.div<{
  width?: string;
  height?: string;
  position?: TPosition;
  dir?: "row" | "column";
  item?: AlignItems;
}>`
  display: flex;
  flex-direction: ${({ dir }) => dir};
  justify-content: space-between;
  align-items: ${props => props.item || "center"};
  width: ${props => props.width};
  height: ${props => props.height};
  position: ${props => props.position};
`;
export const AbsoluteFillView = styled.div<{ index?: number }>`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: ${props => props.index || 0};
`;
