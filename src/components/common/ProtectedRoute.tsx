import { ReactElement } from "react";
import { Route, Redirect, RouteProps } from "react-router-dom";
import { useAuth } from "../../api/AuthContext";

const ProtectedRoute = ({
  component: Component,
  render,
  ...rest
}: RouteProps): ReactElement => {
  const { auth } = useAuth();

  return (
    <Route
      {...rest}
      render={(props: any) => {
        if (!auth?.username) {
          return (
            <Redirect
              to={{
                pathname: "/login",
                // 记录从哪个页跳转到登陆页的，登陆后直接跳转到被拦截的页
                state: { from: props.location },
              }}
            />
          );
        }
        return Component ? <Component {...props} /> : render!(props);
      }}
    />
  );
};

export default ProtectedRoute;
