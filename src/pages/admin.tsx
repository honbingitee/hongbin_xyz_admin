import { FC, ReactElement } from "react";
import { Route, Redirect, Switch } from "react-router-dom";
import { Layout } from "antd";
import Dashboard from "../components/Admin/Dashboard";
import PokerManage from "../components/Admin/PokerManage";
import Side from "../components/Admin/Side";
import Header from "../components/Admin/Header";
import BlogManage from "../components/Admin/BlogManage";
import TagManage from "../components/Admin/TagManage";
import AccountManage from "../components/Admin/AccountManage";
import CommentManage from "../components/Admin/CommentManage";
import UserManage from "../components/Admin/UserManage";
import SourceManage from "../components/Admin/SourceManage";
import TodoListManage from "../components/Admin/TodoListManage";

interface IProps {}

const { Content } = Layout;

const Admin: FC<IProps> = (): ReactElement => {
  return (
    <Layout
      style={{
        height: "100vh",
        overflow: "hidden",
      }}
    >
      <Header />
      <Content style={{ padding: "0 50px", display: "flex" }}>
        <Layout style={{ padding: "24px 0", flex: 1, overflow: "hidden" }}>
          <Side />
          <Content
            style={{ padding: "0 24px", overflowY: "hidden", height: "100%" }}
          >
            <Switch>
              <Redirect from='/admin' exact to='/admin/dashboard' />
              <Route path='/admin/dashboard' component={Dashboard} />
              <Route path='/admin/blogManage' component={BlogManage} />
              <Route path='/admin/pokerManage' component={PokerManage} />
              <Route path='/admin/commentManage' component={CommentManage} />
              <Route path='/admin/tagManage' component={TagManage} />
              <Route path='/admin/todoManage' component={TodoListManage} />
              <Route path='/admin/userManage' component={UserManage} />
              <Route path='/admin/sourceManage' component={SourceManage} />
              <Route path='/admin/accountManage' component={AccountManage} />
            </Switch>
          </Content>
        </Layout>
      </Content>
    </Layout>
  );
};

export default Admin;
